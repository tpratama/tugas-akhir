#include<bits/stdc++.h>
using namespace std;

int idx = 1;

void printHeader() {
    printf("\\begin{table}\n");
    printf("\\centering\n");
    printf("\\begin{tabular}{|c|c|c|} \\hline\n");
    printf("    N & Waktu (detik) & Waktu (log) \\\\ \\hline\n");
}

void printFooter() {
    printf("\\end{tabular}\n");
    printf("\\caption{ Uji Coba Peforma Perkalian Matriks dengan FFT (%d) }\n", idx);
    printf("\\label{tab: uji_coba_fft_%d }\n", idx);
	printf("\\end{table}\n");
}

int main() {
    int n;
    double t;
    char tmp[100];

    scanf("%s %s", tmp, tmp);

    printHeader();

    while(scanf("%d %lf", &n, &t)!=EOF) {
        printf("    %d & %.6lf & %.6lf \\\\ \\hline\n", n, t, log(t));
        if (n%200 == 0 && n<1000 && n>=200) {
            printFooter();
            idx++;
            printf("\n");
            printHeader();
        }
    }
    printFooter();
}
