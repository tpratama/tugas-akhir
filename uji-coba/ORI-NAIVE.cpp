#include <cmath>
#include <cstdio>
#include <algorithm>
#include <vector>
using namespace std;

class FFT;
class cpx;
typedef vector<cpx> VCPX;
typedef vector<int> VI;

double EPS = 0.5;
const double PI = 2*acos(0);

class cpx {
public:
    double a, b;
    cpx() {
        a = 0.0; b = 0.0;
    }
    cpx(double na, double nb) {
        a = na; b = nb;
    }
    const cpx operator+(const cpx &c) const {
        return cpx(a + c.a, b + c.b);
    }
    const cpx operator-(const cpx &c) const {
        return cpx(a - c.a, b - c.b);
    }
    const cpx operator*(const cpx &c) const {
        return cpx(a*c.a - b*c.b, a*c.b + b*c.a);
    }
    double magnitude() {
        return sqrt(a*a+b*b);
    }
};

class FFT {
private:
	vector<cpx> roots;
	vector<int> rev;
	VCPX combinedPoly;

	int s, n;
    void bitReverse(vector<cpx> &array);

public:
	void init(int ns);
	void transform(VCPX& data, bool inverse);
	void massFFT(VCPX& A, VCPX& B);
	void massIFFT(VCPX& A, VCPX& B);
};

void FFT::init(int ns) {
    s = ns;
    n = (1 << s);
    int i, j;
    rev.resize(n);
    roots.resize(n+1);
    combinedPoly.resize(n);
    for (i = 0; i < n; i++)
        for (j = 0; j < s; j++)
            if ((i & (1 << j)) != 0)
                rev[i] += (1 << (s-j-1));
    roots[0] = cpx(1, 0);
    cpx mult = cpx(cos(2*PI/n), sin(2*PI/n));
    for (i = 1; i <= n; i++)
        roots[i] = roots[i-1] * mult;
}

void FFT::bitReverse(vector<cpx> &array) {
    vector<cpx> temp(n);
    int i;
    for (i = 0; i < n; i++) {
        temp[i] = array[rev[i]];
    }
    for (i = 0; i < n; i++)
        array[i] = temp[i];
}

void FFT::transform(VCPX& data, bool f = false) {
    bool inverse = f;

    bitReverse(data);
    int i, j, k;
    for (i = 1; i <= s; i++) {
        int m = (1 << i), md2 = m >> 1;
        int start = 0, increment = (1 << (s-i));
        if (inverse) {
            start = n;
            increment *= -1;
        }
        cpx t, u;
        for (k = 0; k < n; k += m) {
            int index = start;
            for (j = k; j < md2+k; j++) {
                t = roots[index] * data[j+md2];
                index += increment;
                data[j+md2] = data[j] - t;
                data[j] = data[j] + t;
            }
        }
    }
    if (inverse)
        for (i = 0; i < n; i++) {
            data[i].a /= n;
            data[i].b /= n;
        }
}

void FFT::massFFT(VCPX& A, VCPX& B) {
    for(int i=0;i<this->n;i++) {
        combinedPoly[i]=cpx(A[i].a, B[i].a);
    }

    this->transform(combinedPoly);

    A[0] = cpx(combinedPoly[0].a, 0.0);
    B[0] = cpx(combinedPoly[0].b, 0.0);

    for(int i=1;i<=n/2;i++) {
        cpx& positiveRootPart = combinedPoly[i];
        cpx& negativeRootPart = combinedPoly[n-i];

        cpx realPart = positiveRootPart +
                        negativeRootPart;
        cpx imPart = positiveRootPart -
                        negativeRootPart;

        double AReal = realPart.a/2;
        double BReal = realPart.b/2;
        double AIm = imPart.b/2;
        double BIm = imPart.a/2 * -1;

        A[i] = cpx(AReal, AIm);
        B[i] = cpx(BReal, BIm);
        A[n-i] = cpx(AReal, -1*AIm);
        B[n-i] = cpx(BReal, -1*BIm);
    }
}

void FFT::massIFFT(VCPX& A, VCPX& B) {
    for(int i=0;i<this->n;i++) {
        double realPart = A[i].a - B[i].b;
        double imPart = A[i].b + B[i].a;

        combinedPoly[i]=cpx(realPart, imPart);
    }

    this->transform(combinedPoly, 1);

    for(int i=0;i<this->n;i++) {
        A[i] = cpx(combinedPoly[i].a, 0.0);
        B[i] = cpx(combinedPoly[i].b, 0.0);
    }
}

FFT fft;

class SpecialMatrix {
private:
    VCPX r;
    VCPX c0,c1;
    int n;
    int currentSize;

public:
    SpecialMatrix(int n);
    SpecialMatrix();
    SpecialMatrix operator *(SpecialMatrix& rhs);

    void setRow(VCPX row) {
        this->r = row;
    }

    void setFirstCol(VCPX col) {
        this->c0 = col;
    }

    void setSecondCol(VCPX col) {
        this->c1 = col;
    }

    int getSize() {
        return this->currentSize;
    }

    VCPX getRow() {
        return this->r;
    }

    VCPX getFirstColumn() {
        return this->c0;
    }

    VCPX getSecondColumn() {
        return this->c1;
    }

    int getLength() {
        return this->n;
    }

    void print() {
      for(int i=0;i<n;i++) printf("%.0lf ", this->r[i].a);printf("\n");
      for(int i=0;i<n;i++) printf("%.0lf ", this->c0[i].a);printf("\n");
      for(int i=0;i<n;i++) printf("%.0lf ", this->c1[i].a);printf("\n");
    }

    cpx getValueIdx(int x, int y) {
        if (y == 0) return this->c0[x];
        if (x == 0) return this->r[y];

        if (x>=y) {
            return this->c1[x-y+1];
        }
        else {
            return this->r[y-x+1];
        }
    }

    void setValueIdx(int x, int y, cpx val) {
        if (x>=0 && x<=1 && y>=0 && y<=1) {
            this->r[y] = val;
            if (y==1) this->c1[x] = val;
            if (y==0) this->c0[x] = val;

            return;
        }

        if (x==0) {this->r[x] = val; return;}
        if (y==0) {this->c0[x] = val; return;}
        if (y==1) {this->c1[x] = val; return;}

        if (x>=y) {
            this->c1[x-y+1] = val;
        }
        else {
            this->r[y-x+1] = val;
        }

        return;
    }
};

SpecialMatrix::SpecialMatrix(int n) {
    this->n = n;
    this->currentSize=0;
    int pangkat = 0;
    while((1<<pangkat) < ((n<<1) - 1)) {
        pangkat++;
    }
    int powerTwo = 1 << pangkat;
    this->r.resize(powerTwo);
    this->c0.resize(powerTwo);
    this->c1.resize(powerTwo);
    this->currentSize = powerTwo;
}

SpecialMatrix::SpecialMatrix() {
    this->n = 0;
    this->currentSize=0;
}

SpecialMatrix SpecialMatrix::operator *
    (SpecialMatrix& rhs) {

    SpecialMatrix sm(this->n);

    int sz = this->n;
    for(int i=0;i<sz;i++) {
        for(int j=0;j<sz;j++) {
            double res = 0.0;
            for(int k=0;k<sz;k++) {
                res = res + (this->getValueIdx(j,k).a * rhs.getValueIdx(k,i).a);
            }

            int temp = (int) (res + EPS);
            temp = temp % 3;

            res = (double) temp;
            sm.setValueIdx(j, i, cpx(res, 0));
        }
    }

    return sm;
}

int findPowerTwo(int val) {
    int pangkat = 0;

	while((1<<pangkat) < ((val<<1) - 1)) {
		pangkat++;
	}

  return pangkat;
}

SpecialMatrix generateTransformationMatrix
    (int len) {

    SpecialMatrix transformationMatrix(len);

    VCPX r, c0, c1;
    double rowValue, c0Value, c1Value;

    int properSize = (1 << findPowerTwo(len));

    r.resize(properSize);
    c0.resize(properSize);
    c1.resize(properSize);

    for(int i=0;i<len;i++){
        if (i == len -1) {
            rowValue = r[0].a;
        } else if (i == 0) {
            if (len & 1) rowValue = 0;
            else rowValue = 2;
        } else {
            if (i & 1) rowValue = 1;
            else rowValue = 2;
        }

        if (i == 0) {
            if (len & 1) c0Value = 0;
            else c0Value = 2;
        } else if (i == 1){
            if (len & 1) c0Value = 0;
            else c0Value = 2;
        } else {
            if ((len + i) & 1) {
                c0Value=1;
            }
            else {
                c0Value=2;
            }
        }

        if (len > 2) {
            if (i == 0) {
                c1Value = 1;
            } else if (i == 1) {
                c1Value = 1;
            } else if (i == 2){
                c1Value = 2;
            } else {
                c1Value = 0;
            }
        } else {
            c1Value = 2;
        }

        r[i] = cpx(rowValue, 0.0);
        c0[i] = cpx(c0Value, 0.0);
        c1[i] = cpx(c1Value, 0.0);
    }

    transformationMatrix.setRow(r);
    transformationMatrix.setFirstCol(c0);
    transformationMatrix.setSecondCol(c1);

    return transformationMatrix;
}

SpecialMatrix exponent(SpecialMatrix& baseMatrix,
                       int power) {

    bool alreadyInit = false;
    SpecialMatrix res;

    while(power>0){
        if (power & 1 == 1) {
            if (!alreadyInit) {
                alreadyInit = true;
                res = baseMatrix;
            } else {
                res = res * baseMatrix;
            }
        }
        baseMatrix = baseMatrix * baseMatrix;
        power = power >> 1;
    }

    return res;
}

void solve(VCPX& vec, SpecialMatrix& rhs, int n) {
    char solution[n+1];
    int properSize = (1 << findPowerTwo(n));

    VCPX rLhs = vec;
    rLhs.resize(properSize);
    VCPX rRhs = rhs.getRow();
    VCPX c0Lhs = VCPX(properSize);
    c0Lhs[0] = vec[0];

    VCPX c0Rhs = rhs.getFirstColumn();
    VCPX c1Lhs = VCPX(properSize);
    c1Lhs[1] = vec[1];
    VCPX c1Rhs = rhs.getSecondColumn();

    VCPX rLhsOri = rLhs,
         c0LhsOri = c0Lhs,
         c1LhsOri = c1Lhs;

    VCPX rRhsOri = rRhs,
         c0RhsOri = c0Rhs,
         c1RhsOri = c1Rhs;

    VCPX c1Rhs_rev, c0Rhs_rev;

    c1Rhs_rev = c1Rhs;
    reverse(c1Rhs_rev.begin(),
            c1Rhs_rev.begin() + n);
    c0Rhs_rev = c0Rhs;
    reverse(c0Rhs_rev.begin(),
            c0Rhs_rev.begin() + n);

    fft.massFFT(rLhs, rRhs);
    fft.massFFT(c1Rhs_rev, c0Rhs_rev);

    for (int i=0;i<properSize;i++) {
        rRhs[i] = rLhs[i] * rRhs[i];
        c1Rhs_rev[i] = rLhs[i] * c1Rhs_rev[i];
        c0Rhs_rev[i] = rLhs[i] * c0Rhs_rev[i];
    }

    fft.massIFFT(rRhs, c1Rhs_rev);
    fft.transform(c0Rhs_rev, 1);

    VCPX& rc0 = c0Rhs_rev;
    VCPX& rc1 = c1Rhs_rev;
    VCPX& r0r1 = rRhs;

    int highestOrder = n-1;
    for(int i=0;i<n;i++) {
        double currentRowValue;

        if (i==0) {
            currentRowValue = rc0[highestOrder].a;
        } else if (i==1) {
            currentRowValue = rc1[highestOrder].a;
        } else if (i>1 && i<(n-1)) {
            currentRowValue =
            rLhsOri[0].a * rRhsOri[i].a +
            r0r1[i+1].a +
            rc1[n-2+i].a -
            rLhsOri[0].a * rRhsOri[i+1].a -
            rLhsOri[i+1].a * rRhsOri[0].a -
            rLhsOri[i].a * c1RhsOri[1].a -
            rLhsOri[i-1].a * c1RhsOri[0].a;
        } else {
            currentRowValue = rc0[highestOrder].a;
        }

        int temp = (int) (currentRowValue + EPS);
        temp = (temp+3) % 3;

        if (temp==1) solution[i]='G';
        else if (temp==0) solution[i]='R';
        else solution[i]='W';
    }
    solution[n] = '\0';
    puts(solution);
}

VCPX encodeColor(char catsColor[], int N) {
    VCPX encodedColor(N);
    for (int i=0;i<N;i++) {
        if (catsColor[i] == 'W') {
            encodedColor[i] = cpx(2.0, 0.0);
        }
        else if (catsColor[i] == 'G') {
            encodedColor[i] = cpx(1.0, 0.0);
        }
        else {
            encodedColor[i] = cpx(0.0, 0.0);
        }
    }
    return encodedColor;
}

int main() {
	int n, k;
	char cats[50005];
	scanf("%d %d", &n, &k);
	scanf("%s", cats);

	if (n==1) {
	    printf("%s\n", cats);
	    return 0;
	}

	fft.init(findPowerTwo(n));

	VCPX initialColors = encodeColor(cats, n);

	SpecialMatrix mat =
	    generateTransformationMatrix(n);
	mat = exponent(mat, k);

	solve(initialColors, mat, n);
	return 0;
}

